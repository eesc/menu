#!/usr/bin/env python
# -*- coding: utf-8 -*-
f = file('templates/sidebar.html').readlines()
s = "var menuHTML = '"
for line in f:
    l = line.replace('\n', '').strip().replace('../', "' + menuEESCPath + '")
    if l:
        s += l
s += "';"

m = file('menu.js', 'r').read()
h = file('menu.js', 'w')

for line in m.split('\n'):
    #print line
    if 'var menuHTML' in line:
        l = s
    else:
        l = line
    h.write(l + '\n')
print "Arquivo menu.js atualizado!"
