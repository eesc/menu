var menuEESCPath = 'http://www.eesc.usp.br/menu/';
if (window.location.href.indexOf("file:")>-1) {
    menuEESCPath = '../';
}
//var menuEESCPath = '';

/* printed HTML menu from js_menu command */


var menuCSS = '<link rel="stylesheet" href="' + menuEESCPath + 'menu.css">';

var menuHTML = '<div id="eesc-sidebar" class="reset-eesc" style="display: none"><div id="eesc-sidebar-eesc"><h2 style="font-weight: bold;">A EESC</h2><ul><li><a href="http://www.eesc.usp.br/portaleesc/index.php?option=com_content&view=article&id=1&Itemid=139" target="_blank">Diretoria</a></li><li><a href="http://www.eesc.usp.br/portaleesc/index.php?option=com_content&view=article&id=3&Itemid=140" target="_blank">Administração e Colegiados</a></li><li><a href="http://www.eesc.usp.br/portaleesc/index.php?option=com_content&view=article&id=253&Itemid=426" target="_blank">Departamentos</a></li></ul><p style="font-weight: bold; margin-top: 23px;">GRADUAÇÃO</p><ul><li><a href="http://www.eesc.usp.br/portaleesc/index.php?option=com_content&view=article&id=147&Itemid=165" target="_blank">Cursos</a></li><li><a href="http://www.eesc.usp.br/portaleesc/index.php?option=com_content&view=article&id=148&Itemid=166" target="_blank">Formas de Ingresso</a></li></ul><p style="font-weight: bold; margin-top: 23px;">PÓS-GRADUAÇÃO</p><ul><li><a href="http://www.eesc.usp.br/portaleesc/index.php?option=com_content&view=article&id=153&Itemid=174" target="_blank">Programas</a></li><li><a href="http://www.eesc.usp.br/portaleesc/index.php?option=com_content&view=article&id=154&Itemid=175" target="_blank">Processos Seletivos</a></li></ul><p style="font-weight: bold; margin-top: 23px;">PESQUISA</p><ul><li><a href="http://www.eesc.usp.br/portaleesc/index.php?option=com_content&view=article&id=168&Itemid=194" target="_blank">Grupos de Pesquisa</a></li></ul><p style="font-weight: bold; margin-top: 23px;">EXTENSÃO</p><ul><li><a href="http://www.eesc.usp.br/portaleesc/index.php?option=com_content&view=article&id=197&Itemid=205" target="_blank">Parcerias e Inovação</a></li><li><a href="http://www.eesc.usp.br/portaleesc/index.php?option=com_content&view=article&id=173&Itemid=203" target="_blank">Programas e Projetos</a></li></ul><p style="font-weight: bold; margin-top: 23px;">COMUNICAÇÃO</p><ul><li><a href="http://www.eesc.usp.br/portaleesc/index.php?option=com_content&view=category&layout=blog&id=115&Itemid=216" target="_blank">Notícias</a></li><li><a href="http://www.eesc.usp.br/portaleesc/index.php?option=com_jevents&view=week&layout=listevents&Itemid=333" target="_blank">Agenda de eventos</a></li><li><a href="http://www.eesc.usp.br/portaleesc/index.php?option=com_content&view=category&id=105&Itemid=344" target="_blank">Sala de imprensa</a></li></ul><p style="font-weight: bold; margin-top: 23px;">SERVIÇOS</p><ul><li><a href="http://www.eesc.usp.br/portaleesc/index.php?option=com_wrapper&view=wrapper&Itemid=160" target="_blank">Biblioteca</a></li><li><a href="http://www.eesc.usp.br/portaleesc/index.php?option=com_content&view=article&id=566&Itemid=161" target="_blank">Informática</a></li><br><li><a href="http://www.eesc.usp.br/portaleesc/index.php?option=com_contact&view=contact&id=2&Itemid=144" target="_blank">Ouvidoria</a></li><br><li><a href="http://sistemas.eesc.usp.br/" target="_blank">Sistemas EESC</a></li><li><a href="https://webmail.eesc.usp.br/" target="_blank">Webmail EESC</a></li><br><li><a href="http://www.eesc.usp.br/portaleesc/index.php?option=com_contact&view=contact&id=6&Itemid=492" target="_blank">Fale com a EESC</a></li><li><a href="http://www.eesc.usp.br/portaleesc/index.php?option=com_content&view=article&id=1317&Itemid=681" target="_blank">Lista telefonica</a></li></ul></div><div id="eesc-sidebar-usp"><h2>Universidade de São Paulo</h2><ul><li><a href="http://www.usp.br/" target="_blank">USP hoje</a></li><li><a href="http://www5.usp.br/ensino" target="_blank">Ensino</a></li><li><a href="http://www5.usp.br/pesquisa" target="_blank">Pesquisa</a></li><li><a href="http://www5.usp.br/extensao" target="_blank">Extensão</a></li><li><a href="http://www5.usp.br/institucional" target="_blank">Institucional</a></li><li><a href="http://www.usp.br/imprensa" target="_blank">Sala de Imprensa</a></li><li><a href="http://servicos.usp.br" target="_blank">Sistemas USP</a></li><li><a href="http://www5.usp.br/fale-com-a-usp" target="_blank">Fale com a USP</a></li></ul><h2> Mídias USP </h2><ul><li><a href="http://www.usp.br/agen" target="_blank">Agência USP de Notícias</a></li><li><a href="http://www.edusp.com.br" target="_blank">EDUSP</a></li><li style="padding:0px;"><a href="http://iptv.usp.br" target="_blank">IPTV</a></li><li><a href="http://espaber.uspnet.usp.br/jorusp" target="_blank">Jornal da USP</a></li><li><a href="http://www.radio.usp.br" target="_blank">Rádio USP</a></li><li><a href="http://espaber.uspnet.usp.br/espaber" target="_blank">Revista Espaço Aberto</a></li><li><a href="http://www.usp.br/revistausp" target="_blank">Revista USP</a></li><li><a href="http://www.usp.br/tv" target="_blank">TV USP</a></li></ul><h2>Links úteis</h2><ul><li><a href="http://www.reitoria.usp.br" target="_blank">Reitoria</a></li><li><a href="http://www5.usp.br/institucional/escolas-faculdades-e-institutos" target="_blank">Institutos, Faculdades e Escolas</a></li><li><a href="http://www5.usp.br/ensino/graduacao" target="_blank">Graduação</a></li><li><a href="http://www5.usp.br/ensino/pos-graduacao" target="_blank">Pós-graduação</a></li><li><a href="http://www5.usp.br/institucional/cooperacao-internacional" target="_blank">Cooperação internacional</a></li><li><a href="http://webmail.usp.br" target="_blank">Webmail</a></li><li><a href="http://sistemas.usp.br/telefonia/listaTelefonica" target="_blank">Lista telefônica</a></li></ul></div></div><div class="reset-eesc eesc-topbar" role="navigation"><div class="container"><div class="logos"><a class="eesc-logo-usp" href="http://www.usp.br" title="Universidade de São Paulo">USP</a><span>&gt;</span><a class="eesc-logo-eesc" href="http://www.eesc.usp.br" title="Escola de Engenharia de São Carlos">EESC</a></div></a><div class="eesc-desktop eesc-busca"><gcse:search></gcse:search> <button class="btn btn-default eesc-busca-botao"><i class="fa fa-search"></button></div><div class="eesc-desktop eesc-icons-placeholder"></div><div class="eesc-mobile eesc-busca"><a href="#" class="eesc-busca-link" title="Buscar"><i class="fa fa-search"></i></a></div></div></div><div class="eesc-mobile eesc-busca-mobile"><gcse:search></gcse:search></div>';




if (window.jQuery === undefined) {
    alert('Para embutir o Menu EESC você precisa incluir uma biblioteca jQuery!');
} else {
    jQuery(document).ready(function(){


        function css(a) {
            var sheets = document.styleSheets, o = {};
            for (var i in sheets) {
                var rules = sheets[i].rules || sheets[i].cssRules;
                for (var r in rules) {
                    if (a.is(rules[r].selectorText)) {
                        o = jQuery.extend(o, css2json(rules[r].style), css2json(a.attr('style')));
                    }
                }
            }
            return o;
        };

        function css2json(css) {
            var s = {};
            if (!css) return s;
            if (css instanceof CSSStyleDeclaration) {
                for (var i in css) {
                    if ((css[i]).toLowerCase) {
                        s[(css[i]).toLowerCase()] = (css[css[i]]);
                    }
                }
            } else if (typeof css == "string") {
                css = css.split("; ");
                for (var i in css) {
                    var l = css[i].split(": ");
                    s[l[0].toLowerCase()] = (l[1]);
                }
            }
            return s;
        };


        // Thx to http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        };



        var body = jQuery('body');

        var head = jQuery('head');
        head.append(jQuery(menuCSS));

        var children = body.children().detach();
        var fakeBody = jQuery('<div id="fake-body"></div>');

        // clone style
        //fakeBody.css(css(body));
        body.addClass('eesc-body');

        fakeBody.append(children);
        body.append(jQuery(menuHTML));
        body.append(fakeBody);

        var query = getParameterByName('s');
        if (query){
            body.find('.eesc-topbar form input.pesquisar-box').val(query);
        };

        var topbar = jQuery('.eesc-topbar');

        topbar.on('click', '.logos a', function(e){
            e.preventDefault();
            var current = jQuery(e.currentTarget);
            if (body.hasClass('visible') && current.hasClass('active')){
                jQuery('.eesc-topbar .logos a').removeClass('active');
                body.removeClass('visible');
            } else {
                jQuery('.eesc-topbar .logos a').removeClass('active');
                current.addClass('active');
                body.addClass('visible');
                if (current.hasClass('eesc-logo-usp')){
                    jQuery('#eesc-sidebar>div').hide();
                    jQuery('#eesc-sidebar-usp').show();
                } else {
                    jQuery('#eesc-sidebar>div').hide();
                    jQuery('#eesc-sidebar-eesc').show();
                }
            }
        });

        body.on('click', '#fake-body', function(e){
            jQuery('.eesc-topbar .logos a').removeClass('active');
            body.removeClass('visible');
        });

        var icons = jQuery('#eesc-icons');
        var iconsPlaceholder = jQuery('.eesc-icons-placeholder');
        iconsPlaceholder.html(icons.html());


        topbar.on('click', '.eesc-busca-link', function(e){
            e.preventDefault();
            jQuery('.eesc-busca-mobile').toggleClass('visible');
        });

        topbar.on('click', '.eesc-busca-botao', function(e){
            e.preventDefault();
            jQuery('.eesc-busca .gsc-search-button').click();
        });

        function placeholderEESC(tries){
            return function(){
                if (tries == 0) return;
                if (jQuery('input.gsc-input').length > 0) {
                    jQuery('input.gsc-input').attr('placeholder', 'Pesquisar na EESC');
                    return;
                }
                else {
                    tries--;
                    return setTimeout(placeholderEESC(tries), 150);
                }
            }
        }

        (function() {
          var cx = '003498540288267640911:2jsf2n-xnam';
          var gcse = document.createElement('script');
          gcse.type = 'text/javascript';
          gcse.async = true;
          gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
              '//cse.google.com/cse.js?cx=' + cx;
          var s = document.getElementsByTagName('script')[0];
          s.parentNode.insertBefore(gcse, s);
          setTimeout(placeholderEESC(20), 100);
        })();

  });

};
